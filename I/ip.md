# ip

网络配置工具

**ip命令** 用来显示或操纵Linux主机的路由、网络设备、策略路由和隧道，是Linux下较新的功能强大的网络配置工具。

##  语法 

```shell
ip(选项)(参数)
Usage: ip [ OPTIONS ] OBJECT { COMMAND | help }
       ip [ -force ] -batch filename
```

##  选项 

```shell
OBJECT := { link | address | addrlabel | route | rule | neigh | ntable |
       tunnel | tuntap | maddress | mroute | mrule | monitor | xfrm |
       netns | l2tp | macsec | tcp_metrics | token }
       
-V：显示指令版本信息；
-s：输出更详细的信息；
-f：强制使用指定的协议族；
-4：指定使用的网络层协议是IPv4协议；
-6：指定使用的网络层协议是IPv6协议；
-0：输出信息每条记录输出一行，即使内容较多也不换行显示；
-r：显示主机时，不使用IP地址，而使用主机的域名。
```

##  参数 

```shell
OPTIONS := { -V[ersion] | -s[tatistics] | -d[etails] | -r[esolve] |
        -h[uman-readable] | -iec |
        -f[amily] { inet | inet6 | ipx | dnet | bridge | link } |
        -4 | -6 | -I | -D | -B | -0 |
        -l[oops] { maximum-addr-flush-attempts } |
        -o[neline] | -t[imestamp] | -ts[hort] | -b[atch] [filename] |
        -rc[vbuf] [size] | -n[etns] name | -a[ll] }
        
网络对象：指定要管理的网络对象；
具体操作：对指定的网络对象完成具体操作；
help：显示网络对象支持的操作命令的帮助信息。
```

## ip OBJECT 子命令

### ip link

```
# ip link help
Usage: ip link add [link DEV] [ name ] NAME
                   [ txqueuelen PACKETS ]
                   [ address LLADDR ]
                   [ broadcast LLADDR ]
                   [ mtu MTU ] [index IDX ]
                   [ numtxqueues QUEUE_COUNT ]
                   [ numrxqueues QUEUE_COUNT ]
                   type TYPE [ ARGS ]

       ip link delete { DEVICE | dev DEVICE | group DEVGROUP } type TYPE [ ARGS ]

       ip link set { DEVICE | dev DEVICE | group DEVGROUP }
                          [ { up | down } ]
                          [ type TYPE ARGS ]
                          [ arp { on | off } ]
                          [ dynamic { on | off } ]
                          [ multicast { on | off } ]
                          [ allmulticast { on | off } ]
                          [ promisc { on | off } ]
                          [ trailers { on | off } ]
                          [ carrier { on | off } ]
                          [ txqueuelen PACKETS ]
                          [ name NEWNAME ]
                          [ address LLADDR ]
                          [ broadcast LLADDR ]
                          [ mtu MTU ]
                          [ netns { PID | NAME } ]
                          [ link-netns NAME | link-netnsid ID ]
                          [ alias NAME ]
                          [ vf NUM [ mac LLADDR ]
                                   [ vlan VLANID [ qos VLAN-QOS ] [ proto VLAN-PROTO ] ]
                                   [ rate TXRATE ]
                                   [ max_tx_rate TXRATE ]
                                   [ min_tx_rate TXRATE ]
                                   [ spoofchk { on | off} ]
                                   [ query_rss { on | off} ]
                                   [ state { auto | enable | disable} ] ]
                                   [ trust { on | off} ] ]
                                   [ node_guid { eui64 } ]
                                   [ port_guid { eui64 } ]
                          [ xdp { off |
                                  object FILE [ section NAME ] [ verbose ] |
                                  pinned FILE } ]
                          [ master DEVICE ][ vrf NAME ]
                          [ nomaster ]
                          [ addrgenmode { eui64 | none | stable_secret | random } ]
                          [ protodown { on | off } ]
                          [ gso_max_size BYTES ] | [ gso_max_segs PACKETS ]

       ip link show [ DEVICE | group GROUP ] [up] [master DEV] [vrf NAME] [type TYPE]

       ip link xstats type TYPE [ ARGS ]

       ip link afstats [ dev DEVICE ]

       ip link help [ TYPE ]

TYPE := { vlan | veth | vcan | vxcan | dummy | ifb | macvlan | macvtap |
          bridge | bond | team | ipoib | ip6tnl | ipip | sit | vxlan |
          gre | gretap | erspan | ip6gre | ip6gretap | ip6erspan |
          vti | nlmon | team_slave | bond_slave | ipvlan | geneve |
          bridge_slave | vrf | macsec | netdevsim | rmnet }
```

`ip link` 查看和管理链路层的设备，看不到网络层 ip 地址。比如常用的创建网卡别名，网卡启用禁用等操作。




### ip address 
```
# ip address help
Usage: ip address {add|change|replace} IFADDR dev IFNAME [ LIFETIME ]
                                                      [ CONFFLAG-LIST ]
       ip address del IFADDR dev IFNAME [mngtmpaddr]
       ip address {save|flush} [ dev IFNAME ] [ scope SCOPE-ID ]
                            [ to PREFIX ] [ FLAG-LIST ] [ label LABEL ] [up]
       ip address [ show [ dev IFNAME ] [ scope SCOPE-ID ] [ master DEVICE ]
                         [ type TYPE ] [ to PREFIX ] [ FLAG-LIST ]
                         [ label LABEL ] [up] [ vrf NAME ] ]
       ip address {showdump|restore}
IFADDR := PREFIX | ADDR peer PREFIX
          [ broadcast ADDR ] [ anycast ADDR ]
          [ label IFNAME ] [ scope SCOPE-ID ] [ metric METRIC ]
SCOPE-ID := [ host | link | global | NUMBER ]
FLAG-LIST := [ FLAG-LIST ] FLAG
FLAG  := [ permanent | dynamic | secondary | primary |
           [-]tentative | [-]deprecated | [-]dadfailed | temporary |
           CONFFLAG-LIST ]
CONFFLAG-LIST := [ CONFFLAG-LIST ] CONFFLAG
CONFFLAG  := [ home | nodad | mngtmpaddr | noprefixroute | autojoin ]
LIFETIME := [ valid_lft LFT ] [ preferred_lft LFT ]
LFT := forever | SECONDS
TYPE := { vlan | veth | vcan | vxcan | dummy | ifb | macvlan | macvtap |
          bridge | bond | ipoib | ip6tnl | ipip | sit | vxlan | lowpan |
          gre | gretap | erspan | ip6gre | ip6gretap | ip6erspan | vti |
          nlmon | can | bond_slave | ipvlan | geneve | bridge_slave |
          hsr | macsec | netdevsim }
```

### ip addrlabel
todo
### ip route 
```
# ip route help
Usage: ip route { list | flush } SELECTOR
       ip route save SELECTOR
       ip route restore
       ip route showdump
       ip route get [ ROUTE_GET_FLAGS ] ADDRESS
                            [ from ADDRESS iif STRING ]
                            [ oif STRING ] [ tos TOS ]
                            [ mark NUMBER ] [ vrf NAME ]
                            [ uid NUMBER ] [ ipproto PROTOCOL ]
                            [ sport NUMBER ] [ dport NUMBER ]
       ip route { add | del | change | append | replace } ROUTE
SELECTOR := [ root PREFIX ] [ match PREFIX ] [ exact PREFIX ]
            [ table TABLE_ID ] [ vrf NAME ] [ proto RTPROTO ]
            [ type TYPE ] [ scope SCOPE ]
ROUTE := NODE_SPEC [ INFO_SPEC ]
NODE_SPEC := [ TYPE ] PREFIX [ tos TOS ]
             [ table TABLE_ID ] [ proto RTPROTO ]
             [ scope SCOPE ] [ metric METRIC ]
             [ ttl-propagate { enabled | disabled } ]
INFO_SPEC := NH OPTIONS FLAGS [ nexthop NH ]...
NH := [ encap ENCAPTYPE ENCAPHDR ] [ via [ FAMILY ] ADDRESS ]
            [ dev STRING ] [ weight NUMBER ] NHFLAGS
FAMILY := [ inet | inet6 | ipx | dnet | mpls | bridge | link ]
OPTIONS := FLAGS [ mtu NUMBER ] [ advmss NUMBER ] [ as [ to ] ADDRESS ]
           [ rtt TIME ] [ rttvar TIME ] [ reordering NUMBER ]
           [ window NUMBER ] [ cwnd NUMBER ] [ initcwnd NUMBER ]
           [ ssthresh NUMBER ] [ realms REALM ] [ src ADDRESS ]
           [ rto_min TIME ] [ hoplimit NUMBER ] [ initrwnd NUMBER ]
           [ features FEATURES ] [ quickack BOOL ] [ congctl NAME ]
           [ pref PREF ] [ expires TIME ] [ fastopen_no_cookie BOOL ]
TYPE := { unicast | local | broadcast | multicast | throw |
          unreachable | prohibit | blackhole | nat }
TABLE_ID := [ local | main | default | all | NUMBER ]
SCOPE := [ host | link | global | NUMBER ]
NHFLAGS := [ onlink | pervasive ]
RTPROTO := [ kernel | boot | static | NUMBER ]
PREF := [ low | medium | high ]
TIME := NUMBER[s|ms]
BOOL := [1|0]
FEATURES := ecn
ENCAPTYPE := [ mpls | ip | ip6 | seg6 | seg6local ]
ENCAPHDR := [ MPLSLABEL | SEG6HDR ]
SEG6HDR := [ mode SEGMODE ] segs ADDR1,ADDRi,ADDRn [hmac HMACKEYID] [cleanup]
SEGMODE := [ encap | inline ]
ROUTE_GET_FLAGS := [ fibmatch ]
```

**查看路由表：**

```
#  ip route
default via 172.20.0.1 dev eth0 proto dhcp metric 100
10.244.0.0/24 dev cni0 proto kernel scope link src 10.244.0.1
172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 linkdown
172.20.0.0/20 dev eth0 proto kernel scope link src 172.20.0.19 metric 100
```

**添加路由：**

**删除路由：**

### ip rule 
todo
### ip neigh 
todo
### ip ntable
todo
### ip tunnel 
todo
### ip tuntap 
todo
### ip maddress 
todo
### ip mroute 
todo
### ip mrule 
todo
### ip monitor 
todo
### ip xfrm
todo

### ip netns

```
# ip netns help
Usage: ip netns list
       ip netns add NAME
       ip netns set NAME NETNSID
       ip [-all] netns delete [NAME]
       ip netns identify [PID]
       ip netns pids NAME
       ip [-all] netns exec [NAME] cmd ...
       ip netns monitor
       ip netns list-id
```

ip netns 命令用来管理 network namespace。它可以创建命名的 network namespace，然后通过名字来引用 network namespace

network namespace 在逻辑上是网络堆栈的一个副本，它有自己的路由、防火墙规则和网络设备。

默认情况下，子进程继承其父进程的 network namespace。也就是说，如果不显式创建新的 network namespace，所有进程都从 init 进程继承相同的默认 network namespace。

根据约定，命名的 network namespace 是可以打开的 /var/run/netns/ 目录下的一个对象。比如有一个名称为 net1 的 network namespace 对象，则可以由打开 /var/run/netns/net1 对象产生的文件描述符引用 network namespace net1。通过引用该文件描述符，可以修改进程的 network namespace。

**创建 netns：**

`ip netns add NAME` 命令创建一个命名的 network namespace：

```
$ ip netns add neta
$ ip netns list
neta
```

通过下面的演示来理解 ip netns add 命令的本质。

查看默认 network namespace 的 ID：

```
$  readlink /proc/$$/ns/net
net:[4026531992]
```

在 /var/run/netns 目录下创建一个用于绑定 network namespace 的文件，名为 mynet：

```
$ sudo mkdir -p /var/run/netns
$ sudo touch /var/run/netns/mynet
```

通过 unshare 命令创建新的 network namespace，并在新的 namespace 中启动新的 bash：

```
$ sudo unshare --net bash
```
查看新的 network namespace ID：
```
# readlink /proc/$$/ns/net
```

通过绑定挂载把当前 bash 进程的 network namespace 文件挂载到前面创建的 mynet 文件上：

```
# mount --bind /proc/$$/ns/net /var/run/netns/mynet
# ls -I /var/run/netns/mynet
```
通过 ls -I 命令可以看到文件 mynet 的 inode 号和 network namespace 的 ID 相同，说明绑定成功：



退出新创建的 bash，再检查一次 mynet 文件的 inode：
```
# exit
$ ls -I /var/run/netns/mynet
```

可以看出 mynet 文件的 inode 没有发生变化，说明我们使用了绑定挂载后，虽然新的 network namespace 中已经没有进程了，但这个新的 network namespace 还继续存在。

上面的一系列操作其实等同于执行了命令：sudo ip netns add mynet
下面的 nsenter 命令则等同于执行了命令： sudo ip netns exec mynet bash
```
$ sudo nsenter --net=/var/run/netns/mynet bash
# readlink /proc/$$/ns/net
```

通过 nsenter 命令新建了一个 bash 进程，并把它加入 mynet 所关联的 network namespace(net:[4026532616])。

从上面的示例可以看出，创建命名的 network namespace 其实就是创建一个文件，然后通过绑定挂载的方式将新创建的 network namespace 文件(/proc/$$/ns/net)和该文件绑定，就算该 network namespace 里的所有进程都退出了，内核还是会保留该 network namespace，以后我们还可以通过这个绑定的文件来加入该 network namespace。


**查看 netns：**
`ip netns list` 命令显示所有命名的 network namesapce，其实就是显示 /var/run/netns 目录下的所有 network namespace 对象：

```
$ ip netns list
neta
```

**删除 netns：**

`ip [-all] netns del [ NAME ]` 命令删除指定名称的 network namespace。如果指定了 -all 选项，则尝试删除所有的 network namespace。

注意，如果我们把网卡设置到了某个 network namespace 中，并在该 network namespace 中启动了进程：

```
$ sudo ip netns add net0
$ sudo ip link set dev eth0 netns net0
$ sudo ip netns exec net0 bash
```

在另一个 bash 进程中删除 network namespace net0：
```
$ sudo ip netns del net0
```

此时虽然可以删除 netowrk namespace，但是在进程退出之前，网卡一直会保持在你已经删除了的那个 network namespace 中。

**查看进程的 netns：**
ip netns identify [PID] 命令用来查看进程的 network namespace。如果不指定 PID 就显示当前进程的 network namespace：

下面的命令指定了 PID：

**查看 network namespace 中进程的 PID：**
ip netns pids NAME 命令用来查看指定的 network namespace 中的进程的 PID。这个命令其实就是去检查 /proc 下的所有进程，看进程的 network namespace 是不是指定的 network namespace：

**在指定的 network namespace 中执行命令：**
ip [-all] netns exec [ NAME ] cmd 命令用来在指定的 network namespace 中执行命令。比如我们要看一下某个 network namespace 中有哪些网卡：

ip netns exec 后面跟着 namespace 的名字，比如这里的 neta，然后是要执行的命令，只要是合法的 shell 命令都能运行，比如上面的 ip addr 或者 bash。
更棒的是，执行的可以是任何命令，不只是和网络相关的(当然，和网络无关命令执行的结果和在外部执行没有区别)。比如下面例子中，执行 bash 命令之后，后面所有的命令都是在这个 network namespace 中执行的，好处是不用每次执行命令都要把 ip netns exec NAME 补全，缺点是你无法清楚知道自己当前所在的 shell，容易混淆：

通过 -all 参数我们可以同时在所有的 network namespace 执行命令：

输出中的 netns: 指示在某个 network namespace 中执行的结果。

**监控对 network namespace 的操作**
ip netns monitor 命令用来监控对 network namespace 的操作。比如我们删除一个 network namespace 时就会收到相应的通知：

### ip l2tp 
todo
### ip macsec 
todo
### ip tcp_metrics 
todo
### ip token
todo








##  实例 

```shellbash
ip link show                     # 显示网络接口信息
ip link set eth0 up             # 开启网卡
ip link set eth0 down            # 关闭网卡
ip link set eth0 promisc on      # 开启网卡的混合模式
ip link set eth0 promisc offi    # 关闭网卡的混个模式
ip link set eth0 txqueuelen 1200 # 设置网卡队列长度
ip link set eth0 mtu 1400        # 设置网卡最大传输单元
ip addr show     # 显示网卡IP信息
ip addr add 192.168.0.1/24 dev eth0 # 设置eth0网卡IP地址192.168.0.1
ip addr del 192.168.0.1/24 dev eth0 # 删除eth0网卡IP地址

ip route show # 显示系统路由
ip route add default via 192.168.1.254   # 设置系统默认路由
ip route list                 # 查看路由信息
ip route add 192.168.4.0/24  via  192.168.0.254 dev eth0 # 设置192.168.4.0网段的网关为192.168.0.254,数据走eth0接口
ip route add default via  192.168.0.254  dev eth0        # 设置默认网关为192.168.0.254
ip route del 192.168.4.0/24   # 删除192.168.4.0网段的网关
ip route del default          # 删除默认路由
ip route delete 192.168.1.0/24 dev eth0 # 删除路由
```

**用ip命令显示网络设备的运行状态** 

```shell
[root@localhost ~]# ip link list
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 16436 qdisc noqueue
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast qlen 1000
    link/ether 00:16:3e:00:1e:51 brd ff:ff:ff:ff:ff:ff
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast qlen 1000
    link/ether 00:16:3e:00:1e:52 brd ff:ff:ff:ff:ff:ff
```

**显示更加详细的设备信息** 

```shell
[root@localhost ~]# ip -s link list
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 16436 qdisc noqueue
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    RX: bytes  packets  errors  dropped overrun mcast   
    5082831    56145    0       0       0       0      
    TX: bytes  packets  errors  dropped carrier collsns
    5082831    56145    0       0       0       0      
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast qlen 1000
    link/ether 00:16:3e:00:1e:51 brd ff:ff:ff:ff:ff:ff
    RX: bytes  packets  errors  dropped overrun mcast   
    3641655380 62027099 0       0       0       0      
    TX: bytes  packets  errors  dropped carrier collsns
    6155236    89160    0       0       0       0      
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast qlen 1000
    link/ether 00:16:3e:00:1e:52 brd ff:ff:ff:ff:ff:ff
    RX: bytes  packets  errors  dropped overrun mcast   
    2562136822 488237847 0       0       0       0      
    TX: bytes  packets  errors  dropped carrier collsns
    3486617396 9691081  0       0       0       0     
```

**显示核心路由表** 

```shell
[root@localhost ~]# ip route list 
112.124.12.0/22 dev eth1  proto kernel  scope link  src 112.124.15.130
10.160.0.0/20 dev eth0  proto kernel  scope link  src 10.160.7.81
192.168.0.0/16 via 10.160.15.247 dev eth0
172.16.0.0/12 via 10.160.15.247 dev eth0
10.0.0.0/8 via 10.160.15.247 dev eth0
default via 112.124.15.247 dev eth1
```

**显示邻居表** 

```shell
[root@localhost ~]# ip neigh list
112.124.15.247 dev eth1 lladdr 00:00:0c:9f:f3:88 REACHABLE
10.160.15.247 dev eth0 lladdr 00:00:0c:9f:f2:c0 STALE
```

**获取主机所有网络接口**

```shell
ip link | grep -E '^[0-9]' | awk -F: '{print $2}'
```


# 参考
<https://jaywcjlove.github.io/linux-command/>
<https://www.cnblogs.com/sparkdev/p/9253409.html>
<https://man7.org/linux/man-pages/man8/ip-netns.8.html>
<https://man7.org/linux/man-pages/man8/ip-link.8.html>