# brctl

brctl 用来管理以太网桥，在内核中建立，维护，检查网桥配置。

网桥一般用来连接多个不同的网络设备（服务器）的网络设备（网桥），也可以连接多个以太网络，这样这些不同的网络中设备就可以像一个网络那样进行通讯。

网桥是一种在链路层实现中继，对帧进行转发的技术，根据 MAC 分区块，可隔离碰撞，将网络的多个网段在数据链路层连接起来的网络设备。网桥工作在数据链路层，将两个 LAN 连起来，根据 MAC 地址来转发帧，可以看作一个“底层的交换机”

在网桥上每个以太网连接可以对应到一个物理接口，这些以太网接口组合成一个大的逻辑的接口，这个逻辑接口对应于桥接网络。


安装网桥管理工具包：bridge-utile

```
# yum install bridge-utils -y
```



每个网桥都有许多连接端口。这些端口中的任何一个进来都会转发给另一个。端口透明，因此其余部分看不见桥的网络（即，它不会显示在traceroute（8）中）。

## 端口存活检查

网桥跟踪在每个端口上的以太网地址。通过以太网数据帧的头部信息知道将帧转发到哪个端口，并且 it can 'cheat' by forwarding the frame to that port only，从而减少冗余副本和减轻传输流量的损耗。

但是，以太网地址位置数据不是静态数据。机器可以移动到其他端口，可以更换网卡（更改机器的以太网地址）等。

```
brctl showmacs <brname>
```

显示该网桥上已知的 MAC 地址列表。

```
brctl setageing <brname> <time>
```

设置以太网（MAC）地址老化时间，以秒为单位。在 <time> 秒后未看过来自特定地址的帧，网桥将超时（从转发数据库（fdb）中删除）该地址。

```
brctl setgcint <brname> <time>
```

设置垃圾收集时间。这表示网桥将检查转发数据库是否超时每 <time> 秒监测一次。

## STP 生成树协议


多个以太网桥可以工作在一起组成一个更大的网络，利用 802.1d 协议在两个网络之间寻找最短路径，STP 的作用是防止以太网桥之间形成回路，如果确定只有一个网桥，则可以关闭STP。

```
brctl stp <bridge> <state>  
```

控制网桥是否加入 STP 树中，<state> 可以是 'on' 或者 'yes' 表示加入 stp 树中，这样当 lan 中有多个网桥时可以防止回环，'off'表示关闭stp。

```
brctl setbridgeprio <bridge> <priority>
```

设置网桥的优先级，<priority>的值为0-65535，值小的优先级高，优先级最高的是根网桥。

```
brctl setfd <bridge> <time> 
```

设置网桥的'bridge forward delay'转发延迟时间，时间以秒为单位

```
brctl sethello <bridge> <time> 
```

设置网桥的'bridge hello time'存活检测时间

```
brctl setmaxage <bridge> <time>
```

设置网桥的'maximum message age'时间

```
brctl setpathcost <bridge> <port> <cost>
```

设置网桥中某个端口的链路花费值

```
brctl  setportprio  <bridge>  <port> <priority>
```

设置网桥中某个端口的优先级



多个以太网桥可以协同工作，使用 IEEE 802.1d 生成树协议可以创建更大的以太网。

该协议用于查找在两个以太网之间最短路径，并消除来自拓扑的广播封板。由于此协议是标准协议，因此Linux桥接器将
       与其他第三方网桥产品正常互通。
       网桥通过发送和接收相互通信
       BPDU（网桥协议数据单元）。这些BPDU可以被识别
       通过以太网目标地址 01：80：c2：00：00：00。

       生成树协议也可以关闭（对于那些
       只是没有意义的情况，例如何时
       这个Linux盒子是LAN上的唯一桥梁，或者您知道
       拓扑中没有回路。）

       brctl（8）可用于配置某些生成树
       协议参数。有关这些参数的说明，请参见
       IEEE 802.1d规范（或给我发送电子邮件）。默认值
       值应该很好。如果你不知道这些是什么
       参数意味着，您可能不会感到需要调整
       他们。

       brctl stp <bridge> <state>控制此桥实例的
       参与生成树协议。如果<state>为“ on”
       或“是” STP将被打开，否则它将被打开
       离开。关闭后，网桥将不会发送或接收BPDU，
       因此不会参与生成树协议。如果
       您的网桥不是局域网上的唯一网桥，或者
       在局域网拓扑中循环时，请勿关闭此选项。如果你
       关闭此选项，请知道您在做什么。

       brctl setbridgeprio <bridge> <priority>设置桥的
       <priority>的优先级。优先级值为无符号的16位
       数量（0到65535之间的数字），并且没有尺寸。
       较低的优先级值“更好”。最低的桥
       优先级将被选为“根桥”。

       brctl setfd <bridge> <time>设置桥梁的“前进桥梁”
       延迟”到<时间>秒。

       brctl sethello <bridge> <time>设置桥梁的'bridge hello
       时间”到<时间>秒。

       brctl setmaxage <bridge> <time>设置桥的'maximum
       邮件期限”到<时间>秒。

       brctl setpathcost <bridge> <port> <cost>设置端口成本
       端口<port>到<cost>。这是一个无量纲的指标。

       brctl setportprio <bridge> <port> <priority>设置端口
       <端口>的优先级高于<优先级>。优先级值是
       无符号的8位数字（0到255之间的数字），并且没有
       尺寸。此度量标准用于指定的端口和根目录
       端口选择算法。

## 注意

brctl（8）已过时。一些功能，例如 STP guard，harpin mode， fastleave 和 root block 不打算在这个命令中支持。而是推荐使用 iproute2 的 bridge 命令来进行操作。


## 示例

**新建和删除虚拟网桥：**

```
brctl addbr <brname>
brctl delbr <brname>
```

使用 brctl 命令创建和删除网桥 <name> 。

对应的网络接口桥必须先关闭，然后才能删除网桥

**网桥中添加和移除网络端口：**

```
brctl addif <brname> <ifname>
brctl delif <brname> <ifname>
```

将 <ifname> 端口加入网桥 <brname> 中 。

这里需要注意，一个网卡 <ifname> 只能添加到一个网桥中。如果是远程主机，那么 eth0 已经加入到物理交换机了，不能在加入到新创建的虚拟网桥了，如果强制添加会导致断网。

**查询网桥信息：**

```
brctl show
brctl show  <brname>
```

例如：

```
[root@localhost ~]# brctl show
bridge name     bridge id               STP enabled     interfaces
cni0            8000.6e4cd2014939       no              veth05a33997
                                                        veth5c8a14d5
                                                        veth6925d34b
                                                        vethafab477d
docker0         8000.0242dee8482b       no              veth9ef9e7a
                                                        vethb56414c
br666             8000.000000000000       no
```

## 参考

<https://man7.org/linux/man-pages/man8/brctl.8.html>