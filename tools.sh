#!/bin/bash

# get content 
echo '* [文档首页](README.md)'  > SUMMARY.md
echo '* [字母分类索引](az-content.md)' >> SUMMARY.md
echo '* [功能分类索引](role-content.md)' >> SUMMARY.md

for i in {A..Z}
do
    echo "* [$i]($i/README.md)" >> SUMMARY.md
    for j in `ls $i`
    do
        COMNAME=`echo $j|awk -F'.' '{print $1}'`
        if [ "$COMNAME" = "README" ] ;then
             continue
        fi
        echo "  * [${COMNAME}](${i}/${j})" >> SUMMARY.md
    done
done
