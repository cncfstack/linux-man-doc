  # A Linux 命令
  
  | 命令 | 描述
  | -- | --
  | [ab](A/ab.md) | Apache服务器的性能测试工具
  | [accept](A/accept.md) | 指示打印系统接受发往指定目标打印机的打印任务
  | [ack](A/ack.md) | 比grep好用的文本搜索工具
  | [alias](A/alias.md) | 定义或显示别名
  | [apachectl](A/apachectl.md) | Apache服务器前端控制工具
  | [apk](A/apk.md) | Alpine Linux 下的包管理工具
  | [apropos](A/apropos.md) | 在 whatis 数据库中查找字符串
  | [apt-get](A/apt-get.md) | Debian Linux发行版中的APT软件包管理工具
  | [aptitude](A/aptitude.md) | Debian Linux系统中软件包管理工具
  | [apt-key](A/apt-key.md) | 管理Debian Linux系统中的软件包密钥
  | [apt-sortpkgs](A/apt-sortpkgs.md) | Debian Linux下对软件包索引文件进行排序的工具
  | [arch](A/arch.md) | 显示当前主机的硬件架构类型
  | [arj](A/arj.md) | 用于创建和管理.arj压缩包
  | [ar](A/ar.md) | 建立或修改备存文件，或是从备存文件中抽取文件
  | [arpd](A/arpd.md) | 收集免费ARP信息
  | [arping](A/arping.md) | 通过发送ARP协议报文测试网络
  | [arp](A/arp.md) | arp 命令用于显示和修改 IP 到 MAC 转换表
  | [arptables](A/arptables.md) | 管理ARP包过滤规则表
  | [arpwatch](A/arpwatch.md) | 监听网络上ARP的记录
  | [as](A/as.md) | 汇编语言编译器
  | [at](A/at.md) | 在指定时间执行一个任务
  | [atop](A/atop.md) | 监控Linux系统资源与进程的工具
  | [atq](A/atq.md) | 列出当前用户的at任务列表
  | [atrm](A/atrm.md) | 删除待执行任务队列中的指定任务
  | [awk](A/awk.md) | 文本和数据进行处理的编程语言
  | [axel](A/axel.md) | 多线程下载工具